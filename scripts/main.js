/*

Питання 1. Опишіть своїми словами як працює метод forEach.

Відповідь 1. forEach виконує функцію один раз для кожного елементу у масиві.

Питання 2. Як очистити масив?

Відповідь 2. Для очистки масиву можна використати властивість arr.length для масиву зі значенням 0. 

Питання 3. Як можна перевірити, що та чи інша змінна є масивом?

Відповідь 3. Метод arr.isArray() поверне true, якщо об`єкт є масивом та false, якщо він не масив.

*/

"use strict"; //strict mode is a way to introduce better error-checking into your code 

let firstArr = [0, '1654', 'Khakriv', 1654, 'Zalizobeton'];

// function filterBy(arr, type) {
//     let newArr = [];
//     for (let i = 0; i < arr.length; i++) {
//         if (typeof arr[i] !== type) {
//             newArr.push(arr[i]);
//         }

//     }
//     return newArr;
// };

// внесоно зміни - код через Array.filter()

function filterBy(arr, type) {
    let newArr = arr.filter(i => typeof i !== type)
    return newArr
};

console.log(filterBy(firstArr, 'string'));
console.log(filterBy(firstArr, 'number'));
console.log(filterBy(['hello', 'world', 23, '23', null], 'number'));
console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));
